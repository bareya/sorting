# Sorting notes

Review of some sorting algorithms, such as `count`, `radix`, `quick` sort and maybe others in the future.

## Count Sort 

* This version is capable of sorting positive values. 
  For negative values read `An Innovative Counting Sort Algorithm for Negative Numbers` 

* First we create stack array, zero initialized, called `counts`. 

* Size of the array is equal to the biggest number that can be represented by the input.

* For `unsigned char` max value is 255. We have to pick the size of the array with care, because it can't too big. 
  If we want to handle `unsigned int` the max value would be 4,294,967,295, that is definitely too big for stack array.
  
* Next we iterate over the input elements. Value of the current processed element is an index in the `counts` array. 
  Value in `counts` is being incremented.
  
* Last loop is to compute final sorted array. It starts with iterating each element in the `counts`. 
  Value `n` of each element in `counts` indicates how many elements were counted in the input array. 
  Next, index value has to be appended `n` times. 

## Radix Sort

* Implementation is inspired by AMD's publication `Introduction to GPU Radix Sort`, but without parallelism and without 
  involving GPU.
  
* Extended version of `Count Sort`, `unsigned int` input is divided into passes, since we can't have stack array  
  with size `4,294,967,295`.

##  TODO: Quick Sort

* Investigate selection algorithms: `Quickselect`, `Introselect` and `Floyd-Rivest`.
  
* Recommended to read `Introspective Sorting and Selection Algorithms` by `David Musser`   