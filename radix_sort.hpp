/*
MIT License

Copyright (c) 2020 Piotr Barejko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef SORTING_RADIX_SORT_HPP
#define SORTING_RADIX_SORT_HPP


#include <vector>
#include <limits>
#include <vector>
#include <cassert>

template<size_t S, typename T>
void radix_sort(T *input, size_t input_size) {
    static_assert(std::is_unsigned<T>::value, "Radix sort implementation works only with unsigned types");

    constexpr size_t num_bits = std::numeric_limits<T>::digits;
    static_assert((num_bits % S) == 0, "Radix sort does not allow chunk input_size and number to not divide not evenly");

    // number of chunks and bits per chunk
    constexpr size_t num_passes = num_bits / S;

    // previous and current pass
    std::vector<T> prev_temp(input_size);
    T *prev_pass = input;
    T *curr_pass = &prev_temp[0];

    //
    for (size_t pass{}; pass < num_passes; ++pass) {
        constexpr size_t num_pass_bits = num_bits / num_passes;
        constexpr size_t pass_max_value = 1u << num_pass_bits;
        constexpr T pass_bit_mask = (1u << num_pass_bits) - 1;// 1u << 2u = 100, (1u << 2u) - 1 = 011

        const size_t chunk_first_bit = pass * num_pass_bits;

        // count
        size_t counter_table[pass_max_value]{};
        for (size_t i{}; i < input_size; ++i) {
            // right shift to first bit and use mask to filter only bits of interest
            const size_t counter_index = (prev_pass[i] >> chunk_first_bit) & pass_bit_mask;
            assert(counter_index >= 0);
            assert(counter_index < pass_max_value);

            ++counter_table[counter_index];
        }

        // prefix scan
        size_t offset_table[pass_max_value];
        offset_table[0] = 0;
        for (size_t i{1}; i < pass_max_value; ++i) {
            offset_table[i] = offset_table[i - 1] + counter_table[i - 1];
        }

        // reorder
        for (size_t i{}; i < input_size; ++i) {
            const size_t counter_index = (prev_pass[i] >> chunk_first_bit) & pass_bit_mask;
            const size_t new_index = offset_table[counter_index];
            assert(new_index < input_size);

            curr_pass[new_index] = prev_pass[i];
            ++offset_table[counter_index];
        }

        // swap passes
        std::swap(prev_pass, curr_pass);
    }

    // copy
    if (input != prev_pass) {
        for (size_t i{}; i < input_size; ++i) input[i] = prev_pass[i];
    }
}

#endif//SORTING_RADIX_SORT_HPP
